## v0.1.3 / 2020-03-10

### 引入特性

* [支持 Graphviz](https://github.com/88250/liandi/issues/35)

### 改进功能

* [支持隐藏编辑器工具栏](https://github.com/88250/liandi/issues/18)
* [打开选择不挂载，点击关闭按钮失效](https://github.com/88250/liandi/issues/33)
* [支持本地绝对路径渲染图片](https://github.com/88250/liandi/issues/34)
* [支持 ToC](https://github.com/88250/liandi/issues/37)
* [支持脚注](https://github.com/88250/liandi/issues/38)

## v0.1.2 / 2020-02-25

### 改进功能

* [文件名移除 .md](https://github.com/88250/liandi/issues/28)

### 开发重构

* [获取内核进程调试信息](https://github.com/88250/liandi/issues/29)

### 修复缺陷

* [无响应问题](https://github.com/88250/liandi/issues/30)
* [下载太慢，使用自建服务器](https://github.com/88250/liandi/issues/31)

## v0.1.1 / 2020-02-23

### 改进功能

* [更新提示改进](https://github.com/88250/liandi/issues/23)
* [新建文档后选中并打开](https://github.com/88250/liandi/issues/24)
* [macOS 应用菜单细化](https://github.com/88250/liandi/issues/26)
* [多主题改进](https://github.com/88250/liandi/issues/27)

### 修复缺陷

* [工具提示被遮挡](https://github.com/88250/liandi/issues/22)

## v0.1.0 / 2020-02-21

实现笔记应用基础功能，第一次公开发布。
