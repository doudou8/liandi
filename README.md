<p align="center">
<img alt="LianDi" src="https://user-images.githubusercontent.com/873584/74642382-d7352d80-51ad-11ea-8a70-d73beb9534e4.png">
<br>
<em>链滴笔记，连接点滴</em>
<br><br>
<a title="Build Status" target="_blank" href="https://travis-ci.org/88250/liandi"><img src="https://img.shields.io/travis/88250/liandi.svg?style=flat-square"></a>
<a title="Code Size" target="_blank" href="https://github.com/88250/liandi"><img src="https://img.shields.io/github/languages/code-size/88250/liandi.svg?style=flat-square"></a>
<a title="MulanPSL" target="_blank" href="https://github.com/88250/liandi/blob/master/LICENSE"><img src="https://img.shields.io/badge/license-MulanPSL-orange.svg?style=flat-square"></a>
<br>
<a title="Releases" target="_blank" href="https://github.com/88250/liandi/releases"><img src="https://img.shields.io/github/release/88250/liandi.svg?style=flat-square"></a>
<a title="Release Date" target="_blank" href="https://github.com/88250/liandi/releases"><img src="https://img.shields.io/github/release-date/88250/liandi.svg?style=flat-square&color=99CCFF"></a>
<a title="Downloads" target="_blank" href="https://github.com/88250/liandi/releases"><img src="https://img.shields.io/github/downloads/88250/liandi/total.svg?style=flat-square&color=blueviolet"></a>
<br>
<a title="GitHub Commits" target="_blank" href="https://github.com/88250/liandi/commits/master"><img src="https://img.shields.io/github/commit-activity/m/88250/liandi.svg?style=flat-square"></a>
<a title="Last Commit" target="_blank" href="https://github.com/88250/liandi/commits/master"><img src="https://img.shields.io/github/last-commit/88250/liandi.svg?style=flat-square&color=FF9900"></a>
<a title="GitHub Pull Requests" target="_blank" href="https://github.com/88250/liandi/pulls"><img src="https://img.shields.io/github/issues-pr-closed/88250/liandi.svg?style=flat-square&color=FF9966"></a>
<a title="Hits" target="_blank" href="https://github.com/88250/hits"><img src="https://hits.b3log.org/88250/liandi.svg"></a>
<br><br>
<a title="GitHub Watchers" target="_blank" href="https://github.com/88250/liandi/watchers"><img src="https://img.shields.io/github/watchers/88250/liandi.svg?label=Watchers&style=social"></a>  
<a title="GitHub Stars" target="_blank" href="https://github.com/88250/liandi/stargazers"><img src="https://img.shields.io/github/stars/88250/liandi.svg?label=Stars&style=social"></a>  
<a title="GitHub Forks" target="_blank" href="https://github.com/88250/liandi/network/members"><img src="https://img.shields.io/github/forks/88250/liandi.svg?label=Forks&style=social"></a>  
<a title="Author GitHub Followers" target="_blank" href="https://github.com/88250"><img src="https://img.shields.io/github/followers/88250.svg?label=Followers&style=social"></a>
</p>

* [一款桌面端笔记应用，支持 Windows、Mac 和 Linux](https://hacpai.com/article/1582274499427)
* [链滴笔记路线图](https://hacpai.com/article/1579786655216)

欢迎关注 B3log 开源社区微信公众号 `B3log开源`：

![image-d3c00d78](https://user-images.githubusercontent.com/873584/71566370-0d312c00-2af2-11ea-8ea1-0d45d6f0db20.png)

使用截图：

![white](https://user-images.githubusercontent.com/873584/74507339-11e16080-4f37-11ea-8700-e9d4ebfa9787.png)

![dark](https://user-images.githubusercontent.com/873584/74507336-0ee67000-4f37-11ea-827c-903644d0de3e.png)

![dark-md](https://user-images.githubusercontent.com/873584/74507501-89af8b00-4f37-11ea-9de2-534aed8c2c78.png)

![dark-search](https://user-images.githubusercontent.com/873584/74507506-8c11e500-4f37-11ea-9ff2-b1c41b3be225.png)
